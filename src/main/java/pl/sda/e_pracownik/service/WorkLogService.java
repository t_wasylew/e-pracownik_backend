package pl.sda.e_pracownik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.e_pracownik.exceptions.DataBaseException;
import pl.sda.e_pracownik.exceptions.NoWorkLogWithThisIdException;
import pl.sda.e_pracownik.model.WorkLog;
import pl.sda.e_pracownik.repository.WorkLogRepository;
import java.util.Optional;

@Service
public class WorkLogService implements IWorkLogService {

    private WorkLogRepository workLogRepository;

    @Autowired
    public WorkLogService(WorkLogRepository workLogRepository) {
        this.workLogRepository = workLogRepository;
    }

    @Override
    public WorkLog getWorkLogById(Long workLogID) throws DataBaseException {
        Optional<WorkLog> optional = workLogRepository.findById(workLogID);
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new NoWorkLogWithThisIdException();
    }
}
