package pl.sda.e_pracownik.service;

import pl.sda.e_pracownik.model.WorkerInfo;

public interface IWorkerInfoService {
    void updateWorkerInfo(WorkerInfo workerInfo);
}
