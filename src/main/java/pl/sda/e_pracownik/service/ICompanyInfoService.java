package pl.sda.e_pracownik.service;

import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.CompanyInfo;

public interface ICompanyInfoService {
    void update(CompanyInfo companyInfo);

}
