package pl.sda.e_pracownik.service;

import pl.sda.e_pracownik.exceptions.DataBaseException;
import pl.sda.e_pracownik.exceptions.RegistrationException;
import pl.sda.e_pracownik.exceptions.UserDoesNotExistException;
import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.dto.CompanyUserDTO;
import pl.sda.e_pracownik.model.dto.LoginDTO;
import pl.sda.e_pracownik.model.dto.PageResponse;
import pl.sda.e_pracownik.model.dto.WorkerUserDTO;

import java.util.Optional;

public interface IAppUserService {
    PageResponse<AppUser> getAllUsers();
    PageResponse<AppUser> getUsers(int page);

    AppUser getUserById(Long workerID) throws DataBaseException;

    void registerCompanyAccount(CompanyUserDTO companyUserDTO) throws RegistrationException;

    void updateUser(AppUser appUser);

    PageResponse<AppUser> getAllWorkers(Long companyID) throws DataBaseException;

    Optional<Long> registerWorker(WorkerUserDTO workerUserDTO) throws RegistrationException;

    Optional<AppUser> findByLogin(LoginDTO loginDTO) throws UserDoesNotExistException;
}
