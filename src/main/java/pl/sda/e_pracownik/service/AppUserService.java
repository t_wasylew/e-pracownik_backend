package pl.sda.e_pracownik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.e_pracownik.exceptions.*;
import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.CompanyInfo;
import pl.sda.e_pracownik.model.WorkerInfo;
import pl.sda.e_pracownik.model.dto.CompanyUserDTO;
import pl.sda.e_pracownik.model.dto.LoginDTO;
import pl.sda.e_pracownik.model.dto.PageResponse;
import pl.sda.e_pracownik.model.dto.WorkerUserDTO;
import pl.sda.e_pracownik.repository.AppUserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AppUserService implements IAppUserService {

    private AppUserRepository appUserRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;
    private static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    public AppUserService(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public void registerCompanyAccount(CompanyUserDTO companyUserDTO) throws RegistrationException {
        companyUserDTO.setEmail(companyUserDTO.getEmail().toLowerCase());
        companyUserDTO.setLogin(companyUserDTO.getLogin().toLowerCase());

        Optional<AppUser> emailUser = appUserRepository.findByEmail(companyUserDTO.getEmail());
        if (emailUser.isPresent()) {
            throw new UserEmailAlreadyExistsException();
        }
        Optional<AppUser> loginUser = appUserRepository.findByLogin(companyUserDTO.getLogin());
        if (loginUser.isPresent()) {
            throw new UserLoginAlreadyTakenException();
        }

        AppUser appUser = new AppUser();
        appUser.setLogin(companyUserDTO.getLogin());
        appUser.setPassword(encoder.encode(companyUserDTO.getPassword()));
        appUser.setEmail(companyUserDTO.getEmail());
        appUser.setCompanyInfo(new CompanyInfo());
        appUser.getCompanyInfo().setCompanyName(companyUserDTO.getCompanyName());
        appUserRepository.saveAndFlush(appUser);
    }

    @Override
    public AppUser getUserById(Long userID) throws DataBaseException {
        Optional<AppUser> appUser = appUserRepository.findById(userID);
        if (appUser.isPresent()) {
            return appUser.get();
        }
        throw new NoUserWithThisIdException();
    }

    @Override
    public void updateUser(AppUser appUser) {
        appUserRepository.saveAndFlush(appUser);
    }

    @Override
    public PageResponse<AppUser> getAllWorkers(Long companyID) {
        return getWorkers(companyID, 0);
    }

    public PageResponse<AppUser> getWorkers(Long companyID, int currentPage) {
        Optional<AppUser> userOptional = appUserRepository.findById(companyID);
        if (userOptional.isPresent()) {
            AppUser user = userOptional.get();

            List<AppUser> users = user.getCompanyInfo().getWorkers().stream().sorted((o1, o2) -> {
                if (o1.getId() > o2.getId()) return 1;
                else if (o1.getId() < o2.getId()) return -1;
                return 0;
            }).collect(Collectors.toList());

            int totalElements = users.size();
            int totalPages = totalElements / DEFAULT_PAGE_SIZE;

            users = users.stream().skip(currentPage * DEFAULT_PAGE_SIZE).limit(DEFAULT_PAGE_SIZE).collect(Collectors.toList());

            return new PageResponse<>(users, currentPage, totalPages);
        }
        return new PageResponse<>();
    }

    public PageResponse<AppUser> getAllUsers() {
        return getUsers(0);
    }

    public PageResponse<AppUser> getUsers(int page) {
        Page<AppUser> users = appUserRepository.findAllBy(PageRequest.of(page, DEFAULT_PAGE_SIZE));
        return new PageResponse<>(users);
    }

    @Override
    public Optional<Long> registerWorker(WorkerUserDTO workerUserDTO) throws RegistrationException {
        workerUserDTO.setEmail(workerUserDTO.getEmail().toLowerCase());
        workerUserDTO.setLogin(workerUserDTO.getLogin().toLowerCase());

        Optional<AppUser> emailUser = appUserRepository.findByEmail(workerUserDTO.getEmail());
        if (emailUser.isPresent()) {
            throw new UserEmailAlreadyExistsException();
        }
        Optional<AppUser> loginUser = appUserRepository.findByLogin(workerUserDTO.getLogin());
        if (loginUser.isPresent()) {
            throw new UserLoginAlreadyTakenException();
        }

        AppUser appUser = new AppUser();
        appUser.setLogin(workerUserDTO.getLogin());
        appUser.setPassword(encoder.encode(workerUserDTO.getPassword()));
        appUser.setEmail(workerUserDTO.getEmail());
        appUser.setWorkerInfo(new WorkerInfo());
        appUser.getWorkerInfo().setRateType("Hour");
        appUser.getWorkerInfo().setRateValue("1");
        appUser = appUserRepository.saveAndFlush(appUser);
        return Optional.of(appUser.getId());
    }

    @Override
    public Optional<AppUser> findByLogin(LoginDTO dto) throws UserDoesNotExistException {
        Optional<AppUser> foundUser = appUserRepository.findByLogin(dto.getUsername());

        if (!foundUser.isPresent()) {
            throw new UserDoesNotExistException();
        } else {
            AppUser user = foundUser.get(); // wydobywam uzytkownika
            // sprawdzam (nizej) czy haslo zgadza sie z tym z bazy danych
            if (!encoder.matches(dto.getPassword(), user.getPassword())) {
                // jesli nie zgadza sie haslo to exception
                throw new UserDoesNotExistException();

                // jesli sie zgadza to pomijam i zakonczy metodę
            }
        }

        return foundUser;
    }
}
