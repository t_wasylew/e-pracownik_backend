package pl.sda.e_pracownik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.CompanyInfo;
import pl.sda.e_pracownik.repository.CompanyInfoRepository;

@Service
public class CompanyInfoService implements ICompanyInfoService{

    private CompanyInfoRepository companyInfoRepository;

    @Autowired
    public CompanyInfoService(CompanyInfoRepository companyInfoRepository) {
        this.companyInfoRepository = companyInfoRepository;
    }

    @Override
    public void update(CompanyInfo companyInfo) {
        companyInfoRepository.saveAndFlush(companyInfo);
    }


}
