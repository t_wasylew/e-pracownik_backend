package pl.sda.e_pracownik.service;

import pl.sda.e_pracownik.exceptions.DataBaseException;
import pl.sda.e_pracownik.model.WorkLog;

public interface IWorkLogService {
    WorkLog getWorkLogById(Long workLogID) throws DataBaseException;
}
