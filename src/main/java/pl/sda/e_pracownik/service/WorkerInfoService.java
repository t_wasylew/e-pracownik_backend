package pl.sda.e_pracownik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.e_pracownik.model.WorkerInfo;
import pl.sda.e_pracownik.repository.WorkerInfoRepository;

@Service
public class WorkerInfoService implements IWorkerInfoService {

    private WorkerInfoRepository workerInfoRepository;

    @Autowired
    public WorkerInfoService(WorkerInfoRepository workerInfoRepository) {
        this.workerInfoRepository = workerInfoRepository;
    }

    @Override
    public void updateWorkerInfo(WorkerInfo workerInfo) {
        workerInfoRepository.saveAndFlush(workerInfo);
    }
}
