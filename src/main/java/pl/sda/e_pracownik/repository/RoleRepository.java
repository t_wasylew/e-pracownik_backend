package pl.sda.e_pracownik.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.e_pracownik.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
