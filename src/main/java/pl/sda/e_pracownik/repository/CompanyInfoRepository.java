package pl.sda.e_pracownik.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.e_pracownik.model.CompanyInfo;

public interface CompanyInfoRepository extends JpaRepository<CompanyInfo, Long> {
}
