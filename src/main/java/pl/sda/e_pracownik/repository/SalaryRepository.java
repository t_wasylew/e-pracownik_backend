package pl.sda.e_pracownik.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.e_pracownik.model.Salary;

public interface SalaryRepository extends JpaRepository<Salary,Long> {
}
