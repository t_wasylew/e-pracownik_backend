package pl.sda.e_pracownik.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.e_pracownik.model.WorkerInfo;

public interface WorkerInfoRepository extends JpaRepository<WorkerInfo, Long> {

}
