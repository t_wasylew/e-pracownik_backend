package pl.sda.e_pracownik.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.e_pracownik.model.AppUser;

import java.util.List;
import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {


    Optional<AppUser> findByLogin(String login);

    Optional<AppUser> findByEmail(String email);

    Optional<AppUser> findById(Long id);

    Page<AppUser> findAllBy(Pageable pageable);







}
