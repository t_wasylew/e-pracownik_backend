package pl.sda.e_pracownik.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.e_pracownik.model.WorkLog;

public interface WorkLogRepository extends JpaRepository<WorkLog,Long>{
}
