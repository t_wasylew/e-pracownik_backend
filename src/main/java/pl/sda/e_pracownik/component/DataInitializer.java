package pl.sda.e_pracownik.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.Role;
import pl.sda.e_pracownik.repository.AppUserRepository;
import pl.sda.e_pracownik.repository.RoleRepository;

@Component
public class DataInitializer {

    private RoleRepository roleRepository;
    private AppUserRepository appUserRepository;

    @Autowired
    public DataInitializer(RoleRepository roleRepository, AppUserRepository appUserRepository) {
        this.roleRepository = roleRepository;
        this.appUserRepository = appUserRepository;
        loadData();
    }

    private void loadData() {
        Role adminRole = new Role("ADMIN");
        roleRepository.save(new Role("WORKER"));
        roleRepository.save(new Role("COMPANY"));

        appUserRepository.save(new AppUser("admin", "admin", adminRole));
    }
}
