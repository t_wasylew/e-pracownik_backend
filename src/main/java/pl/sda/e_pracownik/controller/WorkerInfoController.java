package pl.sda.e_pracownik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.e_pracownik.exceptions.DataBaseException;
import pl.sda.e_pracownik.exceptions.NoUserWithThisIdException;
import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.Salary;
import pl.sda.e_pracownik.model.WorkLog;
import pl.sda.e_pracownik.model.dto.RespFactory;
import pl.sda.e_pracownik.service.AppUserService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(path = "/worker/")
public class WorkerInfoController {

    private AppUserService appUserService;

    @Autowired
    public WorkerInfoController(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @RequestMapping(path = "/listMyWorkLogs", method = RequestMethod.GET)
    public ResponseEntity<List<WorkLog>> getAllWorkLogsFromUserId(@RequestBody Long workerID) throws DataBaseException {
            AppUser workerAccount = appUserService.getUserById(workerID);
            return RespFactory.result(workerAccount.getWorkerInfo().getWorkLogList());
    }
    @RequestMapping(path = "/listMySalary", method = RequestMethod.GET)
    public ResponseEntity<List<Salary>> getSalaryFromUserId(@RequestBody Long userID) throws DataBaseException {
            AppUser appUser = appUserService.getUserById(userID);
            return RespFactory.result(appUser.getWorkerInfo().getSalaryList());
    }
}


