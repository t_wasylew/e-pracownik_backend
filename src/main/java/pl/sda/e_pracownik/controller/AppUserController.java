package pl.sda.e_pracownik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.e_pracownik.exceptions.DataBaseException;
import pl.sda.e_pracownik.exceptions.RegistrationException;
import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.CompanyInfo;
import pl.sda.e_pracownik.model.dto.*;
import pl.sda.e_pracownik.service.AppUserService;

@RestController
@CrossOrigin
public class AppUserController {

    private AppUserService appUserService;

    @Autowired
    public AppUserController(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @RequestMapping(value = "/registerCompanyAccount", method = RequestMethod.POST)
    public ResponseEntity<Response> registerCompanyAccount(@RequestBody CompanyUserDTO companyUserDTO) {
        try {
            appUserService.registerCompanyAccount(companyUserDTO);
        } catch (RegistrationException e) {
            return RespFactory.badRequest();
        }
        return RespFactory.created();
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseEntity<Response> list() {
        PageResponse<AppUser> list = appUserService.getAllUsers();
        return RespFactory.result(list);
    }

    @RequestMapping(path = "/page", method = RequestMethod.GET)
    public ResponseEntity<Response> page(@RequestParam(name = "page") int page) {
        PageResponse<AppUser> list = appUserService.getUsers(page);
        return RespFactory.result(list);
    }

    @RequestMapping(path = "/getUserByID", method = RequestMethod.GET)
    public ResponseEntity<AppUser> getUserByID(@RequestParam(name = "id") Long id) {
        try {
            AppUser appUser = appUserService.getUserById(id);
            return RespFactory.result(appUser);
        } catch (DataBaseException e) {
            return RespFactory.badRequest();
        }
    }
}
