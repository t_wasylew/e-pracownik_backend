package pl.sda.e_pracownik.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.e_pracownik.exceptions.UserDoesNotExistException;
import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.Role;
import pl.sda.e_pracownik.model.dto.AuthenticationDTO;
import pl.sda.e_pracownik.model.dto.LoginDTO;
import pl.sda.e_pracownik.model.dto.RespFactory;
import pl.sda.e_pracownik.service.AppUserService;
import pl.sda.e_pracownik.service.IAppUserService;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static pl.sda.e_pracownik.configuration.JWTFilter.AUTHORITIES_KEY;
import static pl.sda.e_pracownik.configuration.JWTFilter.SECRET;

@RestController
@CrossOrigin
public class AuthorizationController {

    @Autowired
    private IAppUserService appUserService;

    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<AuthenticationDTO> authenticate(@RequestBody LoginDTO dto) {
        try {
            Optional<AppUser> appUserOptional = appUserService.findByLogin(dto);
            AppUser user = appUserOptional.get();
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
            //We will sign our JWT with our ApiKey secret
            byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET);
            Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
            String token = Jwts.builder()
                    .setSubject(user.getLogin())
                    .setIssuedAt(new Date())
                    .claim(AUTHORITIES_KEY, translateRoles(user.getRoleSet())) // todo: do zmiany na getRoles?
                    .signWith(signatureAlgorithm, signingKey)
                    .compact();
            return RespFactory.result(new AuthenticationDTO(token, user));
        } catch (UserDoesNotExistException e) {
            e.printStackTrace();
        }
        return RespFactory.badRequest();
    }
    private Set<String> translateRoles(Set<Role> roles) {
        return roles.stream().map(role -> role.getName()).collect(Collectors.toSet());
    }
}
