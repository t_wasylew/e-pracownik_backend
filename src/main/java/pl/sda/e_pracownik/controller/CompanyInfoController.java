package pl.sda.e_pracownik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.e_pracownik.exceptions.DataBaseException;
import pl.sda.e_pracownik.exceptions.RegistrationException;
import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.CompanyInfo;
import pl.sda.e_pracownik.model.WorkLog;
import pl.sda.e_pracownik.model.WorkerInfo;
import pl.sda.e_pracownik.model.dto.*;
import pl.sda.e_pracownik.service.AppUserService;
import pl.sda.e_pracownik.service.CompanyInfoService;
import pl.sda.e_pracownik.service.WorkLogService;
import pl.sda.e_pracownik.service.WorkerInfoService;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(path = "/company/")
@CrossOrigin
public class CompanyInfoController {

    private AppUserService appUserService;

    private WorkLogService workLogService;

    private CompanyInfoService companyInfoService;

    private WorkerInfoService workerInfoService;

    @Autowired
    public CompanyInfoController(AppUserService appUserService,
                                 WorkLogService workLogService,
                                 CompanyInfoService companyInfoService,
                                 WorkerInfoService workerInfoService) {
        this.appUserService = appUserService;
        this.workLogService = workLogService;
        this.companyInfoService = companyInfoService;
        this.workerInfoService = workerInfoService;
    }


    @RequestMapping(path = "/addWorkLog", method = RequestMethod.POST)
    public ResponseEntity<Response> addWorkLog(@RequestBody WorkLog workLog, @RequestBody AddWorkLogDTO workLogDTO) throws DataBaseException {
        AppUser companyAccount = appUserService.getUserById(workLogDTO.getCompanyID());
        AppUser workerAccount = appUserService.getUserById(workLogDTO.getWorkerID());
        if (companyAccount.getCompanyInfo() != null && companyAccount.getCompanyInfo().getWorkers().contains(workerAccount)) {
            workerAccount.getWorkerInfo().getWorkLogList().add(workLog);
            workerInfoService.updateWorkerInfo(workerAccount.getWorkerInfo());
            return RespFactory.created();
        }
        return RespFactory.noAuthority();
    }

    @RequestMapping(path = "/editWorkLog", method = RequestMethod.POST)
    public ResponseEntity<Response> editWorkLogForUserWithID(@RequestBody EditWorkLogDTO editWorkLogDTO) throws DataBaseException {
        AppUser companyAccount = appUserService.getUserById(editWorkLogDTO.getCompanyID());
        AppUser workerAccount = appUserService.getUserById(editWorkLogDTO.getWorkerID());
        if (companyAccount.getCompanyInfo() != null && companyAccount.getCompanyInfo().getWorkers().contains(workerAccount)) {

            WorkerInfo info = workerAccount.getWorkerInfo();
            List<WorkLog> workLogs = info.getWorkLogList();

            for (int i = 0; i < workLogs.size(); i++) {
                if (workLogs.get(i).getId().equals(editWorkLogDTO.getWorkLogID())) {
                    workLogs.set(i, editWorkLogDTO.getWorkLog());
                }
            }

            return RespFactory.created();
        }
        return RespFactory.noAuthority();
    }

    @RequestMapping(path = "/setNewRateForWorker", method = RequestMethod.POST)
    public ResponseEntity<Response> changeRate(@RequestBody ChangeRateDTO changeRateDTO) throws DataBaseException {
        AppUser companyAccount = appUserService.getUserById(changeRateDTO.getCompanyID());
        AppUser workerAccount = appUserService.getUserById(changeRateDTO.getWorkerID());
        if (companyAccount.getCompanyInfo() != null && companyAccount.getCompanyInfo().getWorkers().contains(workerAccount)) {
            workerAccount.getWorkerInfo().setRateType(changeRateDTO.getRateType());
            workerAccount.getWorkerInfo().setRateValue(changeRateDTO.getRateValue());
            appUserService.updateUser(workerAccount);
            return RespFactory.created();
        }
        return RespFactory.noAuthority();
    }

    @RequestMapping(path = "/deleteWorkerWithID", method = RequestMethod.POST)
        public ResponseEntity<Response> deleteWorker(@RequestBody DeleteWorkerDTO deleteWorkerDTO) throws DataBaseException {
        AppUser companyAccount = appUserService.getUserById(deleteWorkerDTO.getCompanyID());
        AppUser workerAccount = appUserService.getUserById(deleteWorkerDTO.getUserID());
        if (companyAccount.getCompanyInfo() != null) {
            companyAccount.getCompanyInfo().getWorkers().remove(workerAccount);
            companyInfoService.update(companyAccount.getCompanyInfo());
            return RespFactory.deleted();
        }
        return RespFactory.noAuthority();
    }

    @RequestMapping(path = "/addNewWorker", method = RequestMethod.POST)
    public ResponseEntity<Response> registerNewWorker(@RequestBody RegisterNewWorkerDTO registerNewWorkerDTO) throws DataBaseException {
        AppUser companyAccount = appUserService.getUserById(registerNewWorkerDTO.getCompanyID());
        AppUser workerAccount = appUserService.getUserById(registerNewWorkerDTO.getUserID());
        CompanyInfo companyInfo = companyAccount.getCompanyInfo();
        companyInfo.getWorkers().add(workerAccount);
        companyInfoService.update(companyInfo);
        return RespFactory.created();
    }

    @RequestMapping(value = "/registerWorker", method = RequestMethod.POST)
    public ResponseEntity<Response> registerWorker(@RequestBody WorkerUserDTO workerUserDTO) throws DataBaseException {
        try {
            AppUser companyAccount = appUserService.getUserById(workerUserDTO.getCompanyID());
            if (companyAccount.getCompanyInfo() != null) {
                Optional<Long> workerId = appUserService.registerWorker(workerUserDTO);
                return RespFactory.created(workerId.orElse(-1L));
            }
        } catch (RegistrationException e) {
            return RespFactory.badRequest();
        }
        return RespFactory.noAuthority();
    }


    @RequestMapping(path = "/listWorkers", method = RequestMethod.POST)
    public ResponseEntity<Response> getAllWorkers(@RequestBody Long companyID) {
        PageResponse<AppUser> allWorkers = appUserService.getAllWorkers(companyID);
        return RespFactory.result(allWorkers);
    }

    @RequestMapping(path = "/page", method = RequestMethod.GET)
    public ResponseEntity<Response> page(@RequestParam(name = "companyID") Long companyID, @RequestParam(name = "page") int page) {
        PageResponse<AppUser> list = appUserService.getWorkers(companyID, page);
        return RespFactory.result(list);
    }
}
