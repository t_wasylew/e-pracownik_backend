package pl.sda.e_pracownik.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.apache.catalina.User;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CompanyInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne (mappedBy = "companyInfo")
    @JsonIgnore
    private AppUser companyAccount;

    private String companyName;

    @OneToMany (fetch = FetchType.EAGER)
    @JsonIgnore
    private List<AppUser> workers;

}

