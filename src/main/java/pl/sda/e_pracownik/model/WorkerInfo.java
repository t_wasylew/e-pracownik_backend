package pl.sda.e_pracownik.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkerInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "workerInfo")
    @JsonBackReference
    private AppUser worker;

    @OneToMany( cascade = CascadeType.PERSIST)
    private List<WorkLog> workLogList;

    @OneToMany( cascade = CascadeType.PERSIST)
    private List<Salary> salaryList;

    private String rateType;
    private String rateValue;
}
