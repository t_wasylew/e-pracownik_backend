package pl.sda.e_pracownik.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteWorkerDTO {

    private Long companyID;
    private Long userID;
}
