package pl.sda.e_pracownik.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@NoArgsConstructor
public class PageResponse<T> extends Response {
    private List<T> objects;
    private int currentPage;
    private int totalPages;

    public PageResponse(Page<T> objects) {
        this.currentPage = objects.getNumber();
        this.totalPages = objects.getTotalPages();
        this.objects = objects.getContent();
    }
    public PageResponse(List<T> objects, int currentPage, int totalPages){
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.objects = objects;

    }
}
