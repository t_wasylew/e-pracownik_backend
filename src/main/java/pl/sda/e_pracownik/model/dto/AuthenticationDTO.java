package pl.sda.e_pracownik.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.e_pracownik.model.AppUser;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationDTO {

    private String token;
    private AppUser appUser;
}
