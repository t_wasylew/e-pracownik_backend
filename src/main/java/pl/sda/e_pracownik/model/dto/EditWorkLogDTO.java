package pl.sda.e_pracownik.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.e_pracownik.model.WorkLog;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditWorkLogDTO {

    private Long workerID;
    private Long companyID;
    private Long workLogID;
    private WorkLog workLog;
}
