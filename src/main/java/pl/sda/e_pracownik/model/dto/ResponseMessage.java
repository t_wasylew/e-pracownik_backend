package pl.sda.e_pracownik.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseMessage extends Response {
    private String message;
}
