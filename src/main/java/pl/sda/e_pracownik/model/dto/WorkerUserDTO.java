package pl.sda.e_pracownik.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.e_pracownik.model.WorkerInfo;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkerUserDTO {

    private Long companyID;
    private String login;
    private String password;
    private String email;

}
