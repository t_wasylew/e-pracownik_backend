package pl.sda.e_pracownik.model.dto;

import lombok.Data;

@Data

public class Response {

    private Long timestamp;

    public Response() {
        this.timestamp = System.currentTimeMillis();
    }

}
