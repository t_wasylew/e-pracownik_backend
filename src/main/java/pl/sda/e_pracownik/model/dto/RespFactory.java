package pl.sda.e_pracownik.model.dto;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pl.sda.e_pracownik.model.AppUser;
import pl.sda.e_pracownik.model.WorkLog;

import java.util.List;
import java.util.Set;

public abstract class RespFactory {

    public static ResponseEntity<Response> ok(String message) {
        return ResponseEntity.ok(new ResponseMessage(message));
    }

    public static ResponseEntity created() {
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    public static <T> ResponseEntity created(T t) {
        return ResponseEntity.status(HttpStatus.CREATED).body(t);
    }
    public static ResponseEntity badRequest() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
    public static ResponseEntity noAuthority () {
        String message = "You have no authority";
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(message);
    }

    public static <T> ResponseEntity<Response> result(PageResponse<T> response) {
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    public static ResponseEntity<Response> noResult() {
        return ResponseEntity.notFound().build();
    }

    public static <T> ResponseEntity<T> result(T response) {
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    public static ResponseEntity<Response> deleted() {
        return ResponseEntity.status(HttpStatus.OK).build();

    }
}
