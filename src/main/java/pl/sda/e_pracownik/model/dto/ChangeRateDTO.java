package pl.sda.e_pracownik.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChangeRateDTO {

    private String rateType;
    private String rateValue;
    private Long workerID;
    private Long companyID;
}
