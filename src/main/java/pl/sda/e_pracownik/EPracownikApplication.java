package pl.sda.e_pracownik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EPracownikApplication {

	public static void main(String[] args) {
		SpringApplication.run(EPracownikApplication.class, args);
	}
}
